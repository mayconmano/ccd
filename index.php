<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>CCDigitais SPC</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="HTML5 Template">
        <meta name="description" content="Mist — Multi-Purpose HTML Template">
        <meta name="author" content="zozothemes.com">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico">
        <!-- Font -->
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700,400italic,700italic,800'>
        <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
		<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Montserrat:regular,700' type='text/css' media='all' />

        <!-- Font Awesome Icons -->
        <link href='css/font-awesome/css/font-awesome.min.css' rel='stylesheet' type='text/css'/>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/hover-dropdown-menu.css" rel="stylesheet">
        <!-- Icomoon Icons -->
        <link href="css/icons.css" rel="stylesheet">
        <!-- Revolution Slider -->
        <link href="revolution/css/settings.css" rel="stylesheet">
        <link href="revolution/css/layers.css" rel="stylesheet">
        <link href="revolution/css/navigation.css" rel="stylesheet">
		<!-- Animations -->	
        <link href="css/animate.min.css" rel="stylesheet">
        <!-- Owl Carousel Slider -->
        <link href="css/owl/owl.carousel.css" rel="stylesheet" >
        <link href="css/owl/owl.theme.css" rel="stylesheet" >
        <link href="css/owl/owl.transitions.css" rel="stylesheet" >
        <!-- PrettyPhoto Popup -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
        <!-- Custom Style -->
        <link href="css/style.css" rel="stylesheet">
       <link href="css/responsive.css" rel="stylesheet" />
        <!-- Color Scheme -->
        <link href="css/colors/color1.css" id="changeable-colors" rel="stylesheet">
		

    </head>
    <body>
        <div id="page">		
			<!-- Page Loader -->
			<div id="pageloader">
				<div class="loader-item fa fa-spin text-color"></div>
			</div>
            <!-- transparent header -->
             <div class="new-version tb-pad-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="navbar-header">
                                <!-- Button For Responsive toggle -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span> 
                                <span class="icon-bar"></span> 
                                <span class="icon-bar"></span> 
                                <span class="icon-bar"></span></button> 
                                <!-- Logo -->
                                <a class="navbar-brand" id="logo" href="index.php">
                                <img class="site_logo" alt="Site Logo" src="img/logo.png" />
                                </a>
                            </div>
                            <!-- Navbar Collapse -->
                            <div class="office-details">
                                <div class="detail-box">
                                    <div class="icon"><i class="fa icon-user text-color"></i></div>
                                    <div class="detail">
                                        <a href="login.php"><strong>Acesse sua conta aqui</strong></a>
                                        <span><a href="login.php#register">ou cadastre-se aqui</a></span>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- /.navbar-collapse -->
                        </div>
                        <!-- /.col-md-12 -->
                    </div>
                </div>
            </div>
            <!--  Navbar -->
            <div class="new-version">
				 <!-- Sticky Navbar -->
                <header id="sticker" class="dark-header sticky-navigation">
					<!-- Sticky Menu -->
					<div class="sticky-menu relative">
						<!-- navbar -->
						<div id="navigation" class="navbar navbar-default" role="navigation">
							<div class="container">
								<div class="row">
									<div class="col-md-12">
										<div class="navbar-header">
											<!-- Button For Responsive toggle -->
											<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
											<span class="sr-only">Toggle navigation</span> 
											<span class="icon-bar"></span> 
											<span class="icon-bar"></span> 
											<span class="icon-bar"></span></button>
										</div>
										<!-- Navbar Collapse -->
										<div class="navbar-collapse collapse">
											<!-- nav -->
											<ul class="nav navbar-nav full-width">
												<!-- Home  Mega Menu -->
												<li class="mega-menu">
													<a href="index.php">Home</a>
												</li>
												<li class="mega-menu">
													<a href="comprar_credito.php">Comprar Crédito</a>
												</li>
												<li class="mega-menu">
													<a href="consultas.php">Consultas</a>
												</li>
												<li class="mega-menu">
													<a href="atendimento.php">Atendimento</a>
												</li>
												<li class="mega-menu">
													<a href="login.php">Minha conta</a>
												</li>
																										
											</ul><!-- Right nav -->															
									
											<ul class="navbar-nav bg-color">
												<li class="top-social-icon">							
													Siga-nos:
													<a href="/">
													<i class="fa fa-facebook"></i>
													</a> 
													<a href="/">
													<i class="fa fa-linkedin"></i>
													</a> 
													<a href="/">
													<i class="fa fa-google-plus"></i>
													</a>
												</li>												
											</ul>
											<!-- Right nav -->
											<!-- Header Search Content -->
											<div class="hide-show-content no-display header-search-content">
												<form role="search" class="navbar-form vertically-absolute-middle">
													<div class="form-group">
														<input type="text" placeholder="Enter your text &amp; Search Here"
															class="form-control" id="s" name="s" value="" />
													</div>
												</form>
												<button class="close">
												<i class="fa fa-times"></i>
												</button>
											</div>
											<!-- Header Search Content -->
										</div>
										<!-- /.navbar-collapse -->
									</div>
									<!-- /.col-md-12 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.container -->	
						</div>
					</div>	
				</header>
			</div>
            <!--  Navbar -->
	        <?php /*    <section class="slider" id="home">
				<div id="rev_slider" class="rev_slider" data-version="5.0">
					<ul>
						<!-- Slide -->
						<li data-delay="7000" data-transition="fade" data-slotamount="7" data-masterspeed="2000">							
							<img src="img/sections/slider/3.jpg" alt="" data-bgfit="cover" data-bgposition="center top"
							data-bgrepeat="no-repeat" />
							<div class="elements">
								<h2 class="tp-caption lft skewtotop title bold text-color2" data-x="15" data-y="200"
								data-speed="1000" data-start="1700" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;" 
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"  style="white-space: nowrap;">
									<strong>
									<span class="text-color">Unlock </span> Possibilities</strong>
								</h2>
								<p class="tp-caption lft skewtotop desc1" data-x="15" data-y="280"
								data-speed="1000" data-start="2000" data-transform_in="x:-50px;opacity:0;s:2000;e:Power3.easeOut;" 
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"   style="white-space: nowrap;">
									Strategy that starts with your greatest <br>strengths and builds in execution at every step.
								</p>
								<div class="tp-caption page-scroll lft skewtotop" data-x="15" data-y="370"
								data-speed="1000" data-start="2300" data-transform_in="y:50px;opacity:0;s:1500;e:Power3.easeOut;" 
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"   style="white-space: nowrap;">
									<a href="/get-quote" class="btn btn-default hidden-xs animation animated-item-2">Free Consultation</a>
								</div>
									
							</div>
						</li>
						<!-- Slide Ends -->		
						<!-- Slide -->
						<li data-delay="7000" data-transition="fade" data-slotamount="7" data-masterspeed="2000">							
							<img src="img/sections/slider/5.jpg" alt="" data-bgfit="cover" data-bgposition="center top"
							data-bgrepeat="no-repeat" />
							<div class="elements">
								<h2 class="tp-caption lft skewtotop title bold text-color2" data-x="15" data-y="200"
								data-speed="1000" data-start="1700" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;" 
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"  style="white-space: nowrap;">
									<strong>
									<span class="text-color">Grow Your </span> Business</strong>
								</h2>
								<p class="tp-caption lft skewtotop desc1" data-x="15" data-y="280"
								data-speed="1000" data-start="2000" data-transform_in="x:-50px;opacity:0;s:2000;e:Power3.easeOut;" 
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"   style="white-space: nowrap;">
									Strategy that starts with your greatest <br>strengths and builds in execution at every step.
								</p>
								<div class="tp-caption page-scroll lft skewtotop" data-x="15" data-y="370"
								data-speed="1000" data-start="2300" data-transform_in="y:50px;opacity:0;s:1500;e:Power3.easeOut;" 
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"  style="white-space: nowrap;">
									<a href="/get-quote" class="btn btn-default hidden-xs animation animated-item-2">Our Services</a>
								</div>
									
							</div>
						</li>
						<!-- Slide Ends -->		
					</ul>
					<div class="tp-bannertimer"></div>
				</div></section> */ ?>
			<!-- slider -->
            <section id="services" class="page-section light-bg border-tb">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 bottom-pad-20 col-md-offset-2 text-center" data-animation="fadeInUp">
                            <div class="section-title" data-animation="fadeInUp">
								<!-- Heading -->
								<h2 class="title text-color2">O que você quer consultar?</h2>
							</div>
                        </div>
                    </div>
                     <div class="row">
						<div class="col-sm-6 col-md-4" data-animation="fadeInLeft">
							<div class="item-box style2 icons-color2">
								<a href="/">
									<!-- Icon -->
									<i>R$ 2,75</i>
									<!-- Title -->
									<h5 class="title">Consulta Pessoa Fisíca</h5>
									<!-- Text -->
									<div>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.</div>
									<div class="text-right">
										<a href="produto.php" class="btn btn-default btn-sm animation animated-item-2">+ detalhes</a>
									</div>
								</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-4" data-animation="fadeInUp">
							<div class="item-box style2 icons-color2">
								<a href="/">
									<!-- Icon -->
									<i>R$ 2,75</i>
									<!-- Title -->
									<h5 class="title">Consulta Pessoa Juridica</h5>
									<!-- Text -->
									<div>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.</div>
									<div class="text-right">
										<a href="produto.php" class="btn btn-default btn-sm animation animated-item-2">+ detalhes</a>
									</div>
								</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-4" data-animation="fadeInRight">
							<div class="item-box style2 icons-color2">
								<a href="/">
									<!-- Icon -->
									<i>R$ 2,75</i>
									<!-- Title -->
									<h5 class="title">Consulta Endereço</h5>
									<!-- Text -->
									<div>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.</div>
									<div class="text-right">
										<a href="produto.php" class="btn btn-default btn-sm animation animated-item-2">+ detalhes</a>
									</div>
								</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-4" data-animation="fadeInLeft">
							<div class="item-box style2 icons-color2">
								<a href="/">
									<!-- Icon -->
									<i>R$ 2,75</i>
									<!-- Title -->
									<h5 class="title">Consulta E-mail</h5>
									<!-- Text -->
									<div>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.</div>
									<div class="text-right">
										<a href="produto.php" class="btn btn-default btn-sm animation animated-item-2">+ detalhes</a>
									</div>
								</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-4" data-animation="fadeInRight">
								<div class="item-box style2 icons-color2">
								<a href="/">
									<!-- Icon -->
									<i>R$ 2,75</i>
									<!-- Title -->
									<h5 class="title">Consulta Nome</h5>
									<!-- Text -->
									<div>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.</div>
									<div class="text-right">
										<a href="produto.php" class="btn btn-default btn-sm animation animated-item-2">+ detalhes</a>
									</div>
								</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-4" data-animation="fadeInLeft">
							<div class="item-box style2 icons-color2">
								<a href="/">
									<!-- Icon -->
									<i>R$ 2,75</i>
									<!-- Title -->
									<h5 class="title">Consulta Telefone</h5>
									<!-- Text -->
									<div>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.</div>
									<div class="text-right">
										<a href="produto.php" class="btn btn-default btn-sm animation animated-item-2">+ detalhes</a>
									</div>
								</a>
							</div>
						</div>					
					</div>
				</div>
            </section>
            
			<!-- c2a -->
            <section id="who-we-are" class="page-section">	
				<div class="container who-we-are">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
							<!-- Heading -->
							<h3>Quem nós somos</h3>
							<!-- Text -->
							<p>A CCDigital é o maior distribuidor de consultas do SPC no Brasil.
							Para utilizar nossos serviços basta se cadastrar através da opção comprar crédito. </p>		
							<!-- List -->
							<ul class="arrow-style ls1">
								<li>SPC</li>
								<li>...</li>
								<li>...</li>
							</ul>
							<!-- Link -->
							<a href="/" class="btn btn-default tb-margin-20 bottom-margin-xs-40 animation animated-item-2">Comprar crédito</a>
						</div>					
						<div class="col-md-4 col-sm-6 bottom-margin-md-40 pull-right">
							<!-- Image -->
							<img src="img/sections/about/2.jpg" alt="" title="" width="460" height="480">
						</div>                      
					</div>
                </div>
            </section>
              
			<section id="testimonials" class="page-section bg-color2 testimonials">
                <div class="container white">					
					<div class="section-title" data-animation="fadeInUp">
						<!-- Heading -->
						<h2 class="title">O que as pessoas estão dizendo sobre nós</h2>
					</div>
                    <div class="row">
						<div class="owl-carousel margin-15 text-center" data-pagination="false" data-autoplay="false" data-navigation="false" data-items="3">
							<div class="item">
								<div class="desc-border">
									<blockquote class="small-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec odio ipsum. Suspendisse cursus malesuada facilisis. </blockquote>
									
									<div class="client-details">
										<!-- Name -->
										<strong class="text-color">Sue San</strong> 
										<!-- Company -->
										 
										<span>CEO, zozothemes</span>
									</div>
									 <div class="client-image">
										<!-- Image -->
										<img class="img-circle" src="img/sections/testimonials/1.jpg" width="80" height="80" alt="" />
									</div>
								</div>
							</div>
							<div class="item">
								<div class="desc-border">
									<blockquote class="small-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec odio ipsum. Suspendisse cursus malesuada facilisis. </blockquote>
									
									<div class="client-details">
										<!-- Name -->
										<strong class="text-color">Sam Robe</strong> 
										<!-- Company -->
										 
										<span>Founder, Ramie Glass</span>
									</div>
									 <div class="client-image">
										<!-- Image -->
										<img class="img-circle" src="img/sections/testimonials/2.jpg" width="80" height="80" alt="" />
									</div>
								</div>
							</div>
							<div class="item">
								<div class="desc-border">
									<blockquote class="small-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec odio ipsum. Suspendisse cursus malesuada facilisis. </blockquote>
									
									<div class="client-details">
										<!-- Name -->
										<strong class="text-color">Peter Lain</strong> 
										<!-- Company -->
										 
										<span>Designer, Google</span>
									</div>
									 <div class="client-image">
										<!-- Image -->
										<img class="img-circle" src="img/sections/testimonials/3.jpg" width="80" height="80" alt="" />
									</div>
								</div>
							</div>
							<div class="item">
								<div class="desc-border">
									<blockquote class="small-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec odio ipsum. Suspendisse cursus malesuada facilisis. </blockquote>
									
									<div class="client-details">
										<!-- Name -->
										<strong class="text-color">John Kennedy</strong> 
										<!-- Company -->
										 
										<span>Country Head, Bing</span>
									</div>
									 <div class="client-image">
										<!-- Image -->
										<img class="img-circle" src="img/sections/testimonials/4.jpg" width="80" height="80" alt="" />
									</div>
								</div>
							</div>
							<div class="item">
								<div class="desc-border">
									<blockquote class="small-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec odio ipsum. Suspendisse cursus malesuada facilisis. </blockquote>
									
									<div class="client-details">
										<!-- Name -->
										<strong class="text-color">Rachel Moore</strong> 
										<!-- Company -->
										 
										<span>Marketing, Facebook</span>
									</div>
									 <div class="client-image">
										<!-- Image -->
										<img class="img-circle" src="img/sections/testimonials/5.jpg" width="80" height="80" alt="" />
									</div>
								</div>
							</div>
						</div>					
                    </div>
                </div>
            </section>
            <!-- Testimonials -->
			<section id="clients" class="page-section light-bg border-tb tb-pad-40">
                <div class="container">
                    <div class="row">
						<div class="col-md-4 text-center" data-animation="fadeInLeft">
							<!-- Title -->
							<h4 class="no-margin bottom-margin-md-40 text-color2">Nossos Parceiros:</h4>
						</div>
                        <div class="col-md-8 text-center" data-animation="fadeInRight">
                            <div class="owl-carousel" data-pagination="false" data-items="4" data-tablet="3" data-autoplay="true" data-navigation="false">	
								<!-- Client Images -->
                                <a href="/"><img src="img/sections/clients/1.png" width="120" height="90" alt=""></a>
                                <a href="/"><img src="img/sections/clients/2.png" width="120" height="90" alt=""></a>
                                <a href="/"><img src="img/sections/clients/3.png" width="120" height="90" alt=""></a>
                                <a href="/"><img src="img/sections/clients/4.png" width="120" height="90" alt=""></a>
                                <a href="/"><img src="img/sections/clients/5.png" width="120" height="90" alt=""></a>
                                <a href="/"><img src="img/sections/clients/6.png" width="120" height="90" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- clients -->    
			
			
            <!-- request -->
            <footer id="footer">
                <div class="footer-widget dark-bg white">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 widget bottom-xs-pad-20">
                               <p class="footer-logo">
								<img src="img/logo.png" alt="" width="112" height="66" />
								</p>
                                <!-- Text -->
                                <p> We are experts to provide complete solution for your business with targeted solution and unlock all your possibilities from your existing system and the current system. Don't hesitate to achieve your goals. </p>
                                
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                            	<div class="widget-title">
                                    <!-- Title -->
                                    <h5 class="title">Fale conosco</h5>
                                </div>
                               <!-- Address -->
                                <p><strong>Office:</strong> Zozotheme.com<br> No. 12, Ribbon Building,cWalsh street<br> Sydney, Australia - 2000.</p>
                                <!-- Phone -->
                                <p><strong>Call Us:</strong> +0 (123) 456-78-90<br>
									<strong>Mail: </strong><a href="/">info@zozothemes.com</a>
                                </p>
                                
                            </div>
                           
                            
                            <div class="col-xs-12 col-sm-6 col-md-3 widget newsletter bottom-xs-pad-20">
                                <div class="widget-title">
                                    <!-- Title -->
                                    <h5 class="title">Newsletter Signup</h5>
                                </div>
                                <div>
                                    <!-- Text -->
                                    <p>Subscribe to Our Newsletter to get Important News, Amazing Offers & Inside Scoops:</p>
                                    <p class="form-message1" style="display: none;"></p>
                                    <div class="clearfix"></div>
                                    <!-- Form -->
                                    <form id="subscribe_form" action="http://zozothemes.com/html/bizcon/subscription.php" method="post" name="subscribe_form" role="form">
                                        <div class="input-text form-group has-feedback">
                                            <input class="form-control" type="email" value="" name="subscribe_email">
                                            <button class="submit bg-color" type="submit"><span class="icon-envelop fa"></span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- .newsletter -->
                        </div>
                    </div>
                </div>
                <!-- footer-top -->
                <div class="copyright darkest-bg">
                    <div class="container">
                        <div class="row">
                            <!-- Copyrights -->
                            <div class="col-xs-6 col-md-6">
                                 &copy; 2016 <a href="http://nucleomundialdenegocios.com">Nucleo Mundial de Negócios</a>
                                <br>
                                <!-- Terms Link -->
                                <div class="extra-link"><a href="/">Terms of Use</a> / <a href="/"> Privacy Policy</a></div>
                            </div>
                            <div class="col-xs-6 col-md-6 text-right">
									<!-- Social Links -->
									<div class="social-icon icons-circle i-3x">
										<a href="/">
											<i class="fa fa-facebook border"></i>
										</a> 
										<a href="/">
											<i class="fa fa-linkedin border"></i>
										</a>										
										<a href="/">
											<i class="fa fa-google border"></i>
										</a> 										
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer-bottom -->
            </footer>
            <!-- footer -->
        </div>
        <!-- page -->

        <!-- Scripts -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <!-- Menu jQuery plugin -->
        <script type="text/javascript" src="js/hover-dropdown-menu.js"></script>
        <!-- Menu jQuery Bootstrap Addon -->	
        <script type="text/javascript" src="js/jquery.hover-dropdown-menu-addon.js"></script>	
        <!-- Scroll Top Menu -->
        <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <!-- Sticky Menu -->	
        <script type="text/javascript" src="js/jquery.sticky.js"></script>
        <!-- Bootstrap Validation -->
        <script type="text/javascript" src="js/bootstrapValidator.min.js"></script>
		<!-- Revolution Slider -->
        <script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>   
        <script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script>
			/******************************************
			-	PREPARE PLACEHOLDER FOR SLIDER	-
			******************************************/
			
			var revapi;
			jQuery(document).ready(function() {		
				revapi = jQuery("#rev_slider").revolution({
					sliderType:"standard",
					sliderLayout:"auto",
					delay:9000,
					navigation: {
						arrows:{enable:true}				
					},			
					gridwidth:1230,
					gridheight:680		
				});		
			});	/*ready*/
		</script>	
        <!-- Portfolio Filter -->
        <script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
		<!-- Animations -->
        <script type="text/javascript" src="js/jquery.appear.js"></script>
        <script type="text/javascript" src="js/effect.js"></script>   
		<!-- Charts -->     
		<script type="text/javascript" src="js/charts.js"></script> 
        <!-- Owl Carousel Slider -->
        <script type="text/javascript"  src="js/owl.carousel.min.js"></script>
        <!-- Pretty Photo Popup -->
        <script type="text/javascript"  src="js/jquery.prettyPhoto.js"></script>
        <!-- Parallax BG -->
        <script type="text/javascript"  src="js/jquery.parallax-1.1.3.js"></script>
        <!-- Fun Factor / Counter -->
        <script type="text/javascript"  src="js/jquery.countTo.js"></script>
      
        <!-- Custom Js Code -->
        <script type="text/javascript" src="js/custom.js"></script>
        <!-- Scripts -->
    </body>
</html>