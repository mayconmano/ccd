<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>CCDigitais SPC</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="HTML5 Template">
        <meta name="description" content="Mist — Multi-Purpose HTML Template">
        <meta name="author" content="zozothemes.com">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico">
        <!-- Font -->
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700,400italic,700italic,800'>
        <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
		<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Montserrat:regular,700' type='text/css' media='all' />

        <!-- Font Awesome Icons -->
        <link href='css/font-awesome/css/font-awesome.min.css' rel='stylesheet' type='text/css'/>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/hover-dropdown-menu.css" rel="stylesheet">
        <!-- Icomoon Icons -->
        <link href="css/icons.css" rel="stylesheet">
        <!-- Revolution Slider -->
        <link href="revolution/css/settings.css" rel="stylesheet">
        <link href="revolution/css/layers.css" rel="stylesheet">
        <link href="revolution/css/navigation.css" rel="stylesheet">
		<!-- Animations -->	
        <link href="css/animate.min.css" rel="stylesheet">
        <!-- Owl Carousel Slider -->
        <link href="css/owl/owl.carousel.css" rel="stylesheet" >
        <link href="css/owl/owl.theme.css" rel="stylesheet" >
        <link href="css/owl/owl.transitions.css" rel="stylesheet" >
        <!-- PrettyPhoto Popup -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
        <!-- Custom Style -->
        <link href="css/style.css" rel="stylesheet">
       <link href="css/responsive.css" rel="stylesheet" />
        <!-- Color Scheme -->
        <link href="css/colors/color1.css" id="changeable-colors" rel="stylesheet">
		

    </head>
    <body>
        <div id="page">		
			<!-- Page Loader -->
			<div id="pageloader">
				<div class="loader-item fa fa-spin text-color"></div>
			</div>
            <!-- transparent header -->
             <div class="new-version tb-pad-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="navbar-header">
                                <!-- Button For Responsive toggle -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span> 
                                <span class="icon-bar"></span> 
                                <span class="icon-bar"></span> 
                                <span class="icon-bar"></span></button> 
                                <!-- Logo -->
                                <a class="navbar-brand" id="logo" href="index.php">
                                <img class="site_logo" alt="Site Logo" src="img/logo.png" />
                                </a>
                            </div>
                            <!-- Navbar Collapse -->
                            <div class="office-details">
                                <div class="detail-box">
                                    <div class="icon"><i class="fa icon-user text-color"></i></div>
                                    <div class="detail">
                                        <a href="login.php"><strong>Acesse sua conta aqui</strong></a>
                                        <span><a href="login.php#register">ou cadastre-se aqui</a></span>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- /.navbar-collapse -->
                        </div>
                        <!-- /.col-md-12 -->
                    </div>
                </div>
            </div>
            <!--  Navbar -->
            <div class="new-version">
				 <!-- Sticky Navbar -->
                <header id="sticker" class="dark-header sticky-navigation">
					<!-- Sticky Menu -->
					<div class="sticky-menu relative">
						<!-- navbar -->
						<div id="navigation" class="navbar navbar-default" role="navigation">
							<div class="container">
								<div class="row">
									<div class="col-md-12">
										<div class="navbar-header">
											<!-- Button For Responsive toggle -->
											<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
											<span class="sr-only">Toggle navigation</span> 
											<span class="icon-bar"></span> 
											<span class="icon-bar"></span> 
											<span class="icon-bar"></span></button>
										</div>
										<!-- Navbar Collapse -->
										<div class="navbar-collapse collapse">
											<!-- nav -->
											<ul class="nav navbar-nav full-width">
												<!-- Home  Mega Menu -->
												<li class="mega-menu">
													<a href="index.php">Home</a>
												</li>
												<li class="mega-menu">
													<a href="comprar_credito.php">Comprar Crédito</a>
												</li>
												<li class="mega-menu">
													<a href="consultas.php">Consultas</a>
												</li>
												<li class="mega-menu">
													<a href="atendimento.php">Atendimento</a>
												</li>
												<li class="mega-menu">
													<a href="login.php">Minha conta</a>
												</li>
																										
											</ul><!-- Right nav -->															
									
											<ul class="navbar-nav bg-color">
												<li class="top-social-icon">							
													Siga-nos:
													<a href="/">
													<i class="fa fa-facebook"></i>
													</a> 
													<a href="/">
													<i class="fa fa-linkedin"></i>
													</a> 
													<a href="/">
													<i class="fa fa-google-plus"></i>
													</a>
												</li>												
											</ul>
											<!-- Right nav -->
											<!-- Header Search Content -->
											<div class="hide-show-content no-display header-search-content">
												<form role="search" class="navbar-form vertically-absolute-middle">
													<div class="form-group">
														<input type="text" placeholder="Enter your text &amp; Search Here"
															class="form-control" id="s" name="s" value="" />
													</div>
												</form>
												<button class="close">
												<i class="fa fa-times"></i>
												</button>
											</div>
											<!-- Header Search Content -->
										</div>
										<!-- /.navbar-collapse -->
									</div>
									<!-- /.col-md-12 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.container -->	
						</div>
					</div>	
				</header>
			</div>
            <!--  Navbar -->
	      
           
           <div class="page-header page-title-center">
				<div class="image-bg content-in" data-background="img/back_login.jpg" data-stellar-background-ratio="0.5">
					<div class="overlay-half-dark"></div>
				</div>
				<div class="container white">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 tb-pad-40">
							<!-- Heading -->
							<h1>Login & Cadastro</h1>
							<!-- Sub Heading -->
							<h5>Preencha o formulario abaixo para acessar sua conta.</h5>
							<!-- Form -->
							<form id="login-form" class="login-form form-inline" method="post">
								<div id="success"></div>
								<div class="form-group">
									 <label class="sr-only" for="name">User Name</label>
									 <input class="form-control" type="text" name="name" placeholder="User Name *" /> 
								</div>
								<div class="form-group">
									 <label class="sr-only" for="password">Password</label>
									<input class="form-control" type="text" name="password" placeholder="Password *" />
								</div>
								<button id="submit" class="btn btn-default inverse bsquare no-margin">Sign In</button> 
									<div class="clearfix"></div>
								<div class="col-md-12 top-margin-40 page-scroll">
									<a href="#register" class="text-color">Cadastrar</a> / <a href="#" class="text-color">Esqueceu a senha ?</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- page-header -->
			<section id="register" class="page-section">
                <div class="container">
					<h4>Não tem uma conta? Cadastre-se agora.</h4>
                    <div class="row">
						<div class="col-md-8" data-animation="fadeInLeft">
							 <form id="contact-form" class="contact-form" method="post">
								<div id="success"></div>
								<input class="form-control" type="text" name="name" placeholder="Nome *" /> 
								<input class="form-control" type="email" name="email" placeholder="E-mail *" />
								<div class="row" role="form">
									<div class="col-md-6">
										<input type="text" class="form-control" id="exampleInputEmail2" placeholder="User Name *" />
									</div>
									<div class="col-md-6">
										<input type="text" class="form-control" id="exampleInputPassword2" placeholder="Telefone *" />
									</div>
								</div>
								<div class="row" role="form">
									<div class="col-md-6">
										<input type="text" class="form-control" id="exampleInputEmail2" placeholder="Senha *" />
									</div>
									<div class="col-md-6">
										<input type="text" class="form-control" id="exampleInputPassword2" placeholder="Repita Senha *" />
									</div>
								</div>
								<div class="clearfix"></div>
								<button id="submit" class="btn btn-default">Cadastrar</button> 
								<!-- .buttons-box -->
							</form>
						</div>
						<div class="col-md-4" data-animation="fadeInRight">
							<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.</p>
							<ul class="arrow-style ls1">
								<li><a href="#">Download Template File</a></li>
								<li><a href="#">How to use Template?</a></li>
								<li><a href="#">Navigation Setup</a></li>
								<li><a href="#">Section Customization</a></li>
							</ul>
						</div>	
					</div>
                </div>
            </section>         	 
			
			
            <!-- request -->
            <footer id="footer">
                <div class="footer-widget dark-bg white">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 widget bottom-xs-pad-20">
                               <p class="footer-logo">
								<img src="img/logo.png" alt="" width="112" height="66" />
								</p>
                                <!-- Text -->
                                <p> We are experts to provide complete solution for your business with targeted solution and unlock all your possibilities from your existing system and the current system. Don't hesitate to achieve your goals. </p>
                                
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                            	<div class="widget-title">
                                    <!-- Title -->
                                    <h5 class="title">Fale conosco</h5>
                                </div>
                               <!-- Address -->
                                <p><strong>Office:</strong> Zozotheme.com<br> No. 12, Ribbon Building,cWalsh street<br> Sydney, Australia - 2000.</p>
                                <!-- Phone -->
                                <p><strong>Call Us:</strong> +0 (123) 456-78-90<br>
									<strong>Mail: </strong><a href="/">info@zozothemes.com</a>
                                </p>
                                
                            </div>
                           
                            
                            <div class="col-xs-12 col-sm-6 col-md-3 widget newsletter bottom-xs-pad-20">
                                <div class="widget-title">
                                    <!-- Title -->
                                    <h5 class="title">Newsletter Signup</h5>
                                </div>
                                <div>
                                    <!-- Text -->
                                    <p>Subscribe to Our Newsletter to get Important News, Amazing Offers & Inside Scoops:</p>
                                    <p class="form-message1" style="display: none;"></p>
                                    <div class="clearfix"></div>
                                    <!-- Form -->
                                    <form id="subscribe_form" action="http://zozothemes.com/html/bizcon/subscription.php" method="post" name="subscribe_form" role="form">
                                        <div class="input-text form-group has-feedback">
                                            <input class="form-control" type="email" value="" name="subscribe_email">
                                            <button class="submit bg-color" type="submit"><span class="icon-envelop fa"></span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- .newsletter -->
                        </div>
                    </div>
                </div>
                <!-- footer-top -->
                <div class="copyright darkest-bg">
                    <div class="container">
                        <div class="row">
                            <!-- Copyrights -->
                            <div class="col-xs-6 col-md-6">
                                 &copy; 2016 <a href="http://nucleomundialdenegocios.com">Nucleo Mundial de Negócios</a>
                                <br>
                                <!-- Terms Link -->
                                <div class="extra-link"><a href="/">Terms of Use</a> / <a href="/"> Privacy Policy</a></div>
                            </div>
                            <div class="col-xs-6 col-md-6 text-right">
									<!-- Social Links -->
									<div class="social-icon icons-circle i-3x">
										<a href="/">
											<i class="fa fa-facebook border"></i>
										</a> 
										<a href="/">
											<i class="fa fa-linkedin border"></i>
										</a>										
										<a href="/">
											<i class="fa fa-google border"></i>
										</a> 										
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer-bottom -->
            </footer>
            <!-- footer -->
        </div>
        <!-- page -->

        <!-- Scripts -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <!-- Menu jQuery plugin -->
        <script type="text/javascript" src="js/hover-dropdown-menu.js"></script>
        <!-- Menu jQuery Bootstrap Addon -->	
        <script type="text/javascript" src="js/jquery.hover-dropdown-menu-addon.js"></script>	
        <!-- Scroll Top Menu -->
        <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <!-- Sticky Menu -->	
        <script type="text/javascript" src="js/jquery.sticky.js"></script>
        <!-- Bootstrap Validation -->
        <script type="text/javascript" src="js/bootstrapValidator.min.js"></script>
		<!-- Revolution Slider -->
        <script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>   
        <script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script>
			/******************************************
			-	PREPARE PLACEHOLDER FOR SLIDER	-
			******************************************/
			
			var revapi;
			jQuery(document).ready(function() {		
				revapi = jQuery("#rev_slider").revolution({
					sliderType:"standard",
					sliderLayout:"auto",
					delay:9000,
					navigation: {
						arrows:{enable:true}				
					},			
					gridwidth:1230,
					gridheight:680		
				});		
			});	/*ready*/
		</script>	
        <!-- Portfolio Filter -->
        <script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
		<!-- Animations -->
        <script type="text/javascript" src="js/jquery.appear.js"></script>
        <script type="text/javascript" src="js/effect.js"></script>   
		<!-- Charts -->     
		<script type="text/javascript" src="js/charts.js"></script> 
        <!-- Owl Carousel Slider -->
        <script type="text/javascript"  src="js/owl.carousel.min.js"></script>
        <!-- Pretty Photo Popup -->
        <script type="text/javascript"  src="js/jquery.prettyPhoto.js"></script>
        <!-- Parallax BG -->
        <script type="text/javascript"  src="js/jquery.parallax-1.1.3.js"></script>
        <!-- Fun Factor / Counter -->
        <script type="text/javascript"  src="js/jquery.countTo.js"></script>
      
        <!-- Custom Js Code -->
        <script type="text/javascript" src="js/custom.js"></script>
        <!-- Scripts -->
    </body>
</html>